package utils

import "fmt"

var DEBUG = true

func Println(a ...interface{}) {
	if DEBUG {
		fmt.Println(a...)
	}
}
func Printf(format string, a ...interface{}) {
	if DEBUG {
		fmt.Printf(format, a...)
	}
}
